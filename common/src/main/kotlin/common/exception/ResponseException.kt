package common.exception

import org.springframework.http.HttpStatus
import java.lang.RuntimeException

open class ResponseException(
  val code: HttpStatus,
  message: String
) : RuntimeException(message)