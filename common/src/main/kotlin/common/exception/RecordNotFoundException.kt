package common.exception

import org.springframework.http.HttpStatus

open class RecordNotFoundException(
  message: String
) : ResponseException(HttpStatus.NOT_FOUND, message)