package common

data class ValidationError(
  val field: String,
  val errors: List<String>
)